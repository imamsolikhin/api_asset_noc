<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

/**
 * Route for Documentation.
 */
$router->get('/', function () {
    return redirect('https://documenter.getpostman.com/view/11413602/TVCcYA5c');
});

/**
 * Route Group for API.
 */
$router->group(['prefix' => 'api', 'namespace' => 'API', 'middleware' => 'api'], function () use ($router) {
    /**
     * Route Group for Version 1.
     */
    $router->group(['prefix' => 'v1', 'namespace' => 'v1', 'middleware' => 'signature'], function () use ($router) {
        /**
         * Route for Asset.
         */
        $router->group(['prefix' => '{table}'], function () use ($router) {
            $router->get('/', [
                'as' => 'asset.index', 'uses' => 'AssetController@index'
            ]);
            $router->post('/', [
                'as' => 'asset.store', 'uses' => 'AssetController@store'
            ]);
            $router->post('ajax_list', [
                'as' => 'asset.ajax_list', 'uses' => 'AssetController@ajax_list'
            ]);
            $router->get('{id}', [
                'as' => 'asset.show', 'uses' => 'AssetController@show'
            ]);
            $router->put('{id}', [
                'as' => 'asset.update', 'uses' => 'AssetController@update'
            ]);
            $router->delete('{id}', [
                'as' => 'asset.destroy', 'uses' => 'AssetController@destroy'
            ]);
        });
    });
});
