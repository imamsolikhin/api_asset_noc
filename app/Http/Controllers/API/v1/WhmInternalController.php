<?php

namespace App\Http\Controllers\API\v1;

use App\Http\Models\WhmInternal;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class WhmInternalController extends Controller {

    /**
     * Inkombizz | inkombizz@gmail.com | inkombizz.com
     * Set Queries for Search.
     *
     * @param  Illuminate\Database\Eloquent\Builder  $query
     * @param  string  $key
     * @return Illuminate\Database\Eloquent\Builder
     */
    public static function searchQuery($query, $request) {
       $query->select([
             'ivt_warehouse_mutation_internal.Code AS Code',
             'ivt_warehouse_mutation_internal.BranchCode AS BranchCode',
             'ivt_warehouse_mutation_internal.CompanyCode AS CompanyCode',
             'ivt_warehouse_mutation_internal.ExchangeRate AS ExchangeRate',
             'ivt_warehouse_mutation_internal.Transactiondate AS Transactiondate',
             'ivt_warehouse_mutation_internal.SourceWarehouseCode AS SourceWarehouseCode',
             'ivt_warehouse_mutation_internal.DestinationWarehouseCode AS DestinationWarehouseCode',
             'ivt_warehouse_mutation_internal.ApprovalStatus AS ApprovalStatus',
             'ivt_warehouse_mutation_internal.ApprovalDate AS ApprovalDate',
             'ivt_warehouse_mutation_internal.ApprovalBy AS ApprovalBy',
             'ivt_warehouse_mutation_internal.RefNo AS RefNo',
             'ivt_warehouse_mutation_internal.Remark AS Remark',
       ]);
       return $query;
    }

    /**
     * Inkombizz | inkombizz@gmail.com | inkombizz.com
     * Set Queries for Search.
     *
     * @param  Illuminate\Database\Eloquent\Builder  $query
     * @param  string  $key
     * @return Illuminate\Database\Eloquent\Builder
     */
    public static function searchId($table, $id) {
        $data = getModelName($table)::select([
            'ivt_warehouse_mutation_internal.Code AS Code',
            'ivt_warehouse_mutation_internal.BranchCode AS BranchCode',
            'ivt_warehouse_mutation_internal.CompanyCode AS CompanyCode',
            'ivt_warehouse_mutation_internal.ExchangeRate AS ExchangeRate',
            'ivt_warehouse_mutation_internal.Transactiondate AS Transactiondate',
            'ivt_warehouse_mutation_internal.SourceWarehouseCode AS SourceWarehouseCode',
            'ivt_warehouse_mutation_internal.DestinationWarehouseCode AS DestinationWarehouseCode',
            'ivt_warehouse_mutation_internal.ApprovalStatus AS ApprovalStatus',
            'ivt_warehouse_mutation_internal.ApprovalDate AS ApprovalDate',
            'ivt_warehouse_mutation_internal.ApprovalBy AS ApprovalBy',
            'ivt_warehouse_mutation_internal.RefNo AS RefNo',
            'ivt_warehouse_mutation_internal.Remark AS Remark',
                  ])
            ->find(str_replace('%20', ' ', $id));
              return $data;
    }

    /**
     * Inkombizz | inkombizz@gmail.com | inkombizz.com
     * Set Validation From Request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string|null  $type
     * @return Illuminate\Validation\Validator
     */
    public static function validation($request, $type = null) {
        $rules = [
            'BranchCode' => 'nullable|max:250',
            'CompanyCode' => 'nullable|max:250',
            'CurrencyCode' => 'nullable|max:250',
            'CurrencyCode' => 'nullable|max:250',
            'Transactiondate' => 'nullable|max:250',
            'SourceWarehouseCode' => 'nullable|max:250',
            'DestinationWarehouseCode' => 'nullable|max:250',
            'ApprovalStatus' => 'nullable|max:250',
            'ApprovalDate' => 'nullable|max:250',
            'ApprovalBy' => 'nullable|max:250',
            'RefNo' => 'nullable|max:250',
            'Remark' => 'nullable|max:250',
            'CreatedBy' => 'nullable||max:250',
            'CreatedDate' => 'nullable|date_format:Y-m-d H:i:s',
            'UpdatedBy' => 'nullable|max:250',
            'UpdatedDate' => 'nullable|date_format:Y-m-d H:i:s',
        ];

        return Validator::make($request->all(), $rules);
    }

    /**
     * Inkombizz | inkombizz@gmail.com | inkombizz.com
     * Save resource in database.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  object|null  $data
     * @return object
     */
    public static function save($request, $data = null, $table =null) {
        if (is_null($data)) {
            $data = new WhmInternal;
        }

        if ($request->Code) {
            $data->Code = $request->Code;
        }else {
            $data->Code = generadeCode($table,"WHT-I",null,5);
        }
        if ($request->except('BranchCode')) {
            $data->BranchCode = $request->BranchCode;
        }
        if ($request->except('CompanyCode')) {
            $data->CompanyCode = $request->CompanyCode;
        }
        if ($request->except('CurrencyCode')) {
            $data->CurrencyCode = $request->CurrencyCode;
        }
        if ($request->except('ExchangeRate')) {
            $data->ExchangeRate = $request->ExchangeRate;
        }
        if ($request->except('Transactiondate')) {
            $data->Transactiondate = $request->Transactiondate;
        }
        if ($request->except('SourceWarehouseCode')) {
            $data->SourceWarehouseCode = $request->SourceWarehouseCode;
        }
        if ($request->except('DestinationWarehouseCode')) {
            $data->DestinationWarehouseCode = $request->DestinationWarehouseCode;
        }
        if ($request->except('ApprovalStatus')) {
            $data->ApprovalStatus = $request->ApprovalStatus;
        }
        if ($request->except('ApprovalDate')) {
            $data->ApprovalDate = $request->ApprovalDate;
        }
        if ($request->except('ApprovalBy')) {
            $data->ApprovalBy = $request->ApprovalBy;
        }
        if ($request->except('RefNo')) {
            $data->RefNo = $request->RefNo;
        }
        if ($request->except('Remark')) {
            $data->Remark = $request->Remark;
        }
        if ($request->except('CreatedBy')) {
            $data->CreatedBy = $request->CreatedBy;
        }
        if ($request->except('CreatedDate')) {
            $data->CreatedDate = $request->CreatedDate;
        }
        if ($request->except('UpdatedBy')) {
            $data->UpdatedBy = $request->UpdatedBy;
        }
        if ($request->except('UpdatedDate')) {
            $data->UpdatedDate = $request->UpdatedDate;
        }
        $data->save();

        return $data;
    }

}
