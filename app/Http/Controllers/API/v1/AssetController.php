<?php

namespace App\Http\Controllers\API\v1;

use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;

class AssetController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @param  string  $table
     * @param  \Illuminate\Http\Request  $request
     * @return json
     */
    public function index($table, Request $request) {
        $validator = Validator::make($request->all(), [
                    'limit' => 'required_with:page|integer|min:1',
                    'page' => 'required_with:limit|integer|min:1',
                    'search' => 'nullable',
        ]);

        if ($validator->fails())
            return makeResponse(400, 'error', 'validation is invalid', $validator->errors()->all());

        $data = getModelName($table)::when($request, function ($query, $key) use ($table) {
                    return getControllerName($table)::searchQuery($query, $key, true);
                })->paginate($request->limit);

        return makeResponse(200, 'pagination', null, getResourceName($table)::collection($data));
    }

    /**
     * Display a listing of the resource.
     *
     * @param  string  $table
     * @param  \Illuminate\Http\Request  $request
     * @return datatable json https://datatables.yajrabox.com/starter
     */
    public function ajax_list($table, Request $request) {
        $datatables = getControllerName($table)::createQueryFilter($table, $request);
        return $datatables->make(true);
    }

    /**
     * Store a newly created resource in database.
     *
     * @param  string  $table
     * @param  \Illuminate\Http\Request  $request
     * @return json
     */
    public function store($table, Request $request) {
        $validator = getControllerName($table)::validation($request);

        if ($validator->fails()) {
            return makeResponse(400, 'error', 'validation is invalid', $validator->errors()->all());
        }

        $data = getControllerName($table)::save($request, Null , $table);

        $resource = getResourceName($table);

        Log::info('[' . $data->getTable() . '.create] ' . json_encode(array_merge(['ClientIP' => $request->ip()], ['Data' => new $resource($data)])));

        return makeResponse(200, 'success', 'new ' . str_replace('-', ' ', $table) . ' has been save successfully', new $resource($data));
    }

    /**
     * Display the specified resource.
     *
     * @param  string  $table
     * @param  string  $id
     * @return json
     */
    public function show($table, $id) {
        $data = getModelName($table)::find(str_replace('%20', ' ', $id));

        if (!$data)
            return makeResponse(404, 'error', str_replace('-', ' ', $table) . ' not found');

        $resource = getResourceName($table);

        return makeResponse(200, 'success', null, new $resource($data));
    }

    /**
     * Update the specified resource in database.
     *
     * @param  string  $table
     * @param  \Illuminate\Http\Request  $request
     * @param  string  $id
     * @return json
     */
    public function update($table, Request $request, $id) {
        $data = getModelName($table)::find(str_replace('%20', ' ', $id));

        if (!$data) {
            return makeResponse(404, 'error', str_replace('-', ' ', $table) . ' not found');
        }

        $validator = getControllerName($table)::validation($request, 'update');

        if ($validator->fails()) {
            return makeResponse(400, 'error', 'validation is invalid', $validator->errors()->all());
        }

        $resource = getResourceName($table);

        Log::info('[' . $data->getTable() . '.edit] ' . json_encode(array_merge(['ClientIP' => $request->ip()], ['OldData' => new $resource($data), 'NewData' => array_merge([$data->getKeyName() => str_replace('%20', ' ', $id)], $request->all())])));

        $data = getControllerName($table)::save($request, $data, $table);

        return makeResponse(200, 'success', str_replace('-', ' ', $table) . ' has been update successfully', new $resource($data));
    }

    /**
     * Remove the specified resource from database.
     *
     * @param  string  $table
     * @param  string  $id
     * @return json
     */
    public function destroy($table, Request $request,$id) {
      if (isset($request->HeaderCode)){
        $data = getModelName($table)::where('HeaderCode', '=', $request->HeaderCode)->delete();
      }else{
        $data = getModelName($table)::find(str_replace('%20', ' ', $id));

        if (! $data)
            return makeResponse(404, 'error', str_replace('-', ' ', $table) . ' not found');

        $resource = getResourceName($table);

        Log::info('[' . $data->getTable() . '.delete] ' . json_encode(array_merge([
            'ClientIP' => request()->ip()
        ], [
            'Data' => new $resource($data)
        ])));

        $data->delete();
      }

      return makeResponse(200, 'success', str_replace('-', ' ', $table) . ' has been delete successfully');
  }
}
