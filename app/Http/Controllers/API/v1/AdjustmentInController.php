<?php

namespace App\Http\Controllers\API\v1;

use App\Http\Models\AdjustmentIn;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class AdjustmentInController extends Controller {

    /**
     * Inkombizz | inkombizz@gmail.com | inkombizz.com
     * Set Queries for Search.
     *
     * @param  Illuminate\Database\Eloquent\Builder  $query
     * @param  string  $key
     * @return Illuminate\Database\Eloquent\Builder
     */
    public static function searchQuery($query, $request) {
       $query->select([
             'ivt_adjustment_in.Code AS Code',
             'ivt_adjustment_in.BranchCode AS BranchCode',
             'ivt_adjustment_in.CompanyCode AS CompanyCode',
             'ivt_adjustment_in.Transactiondate AS Transactiondate',
             'ivt_adjustment_in.Warehousecode AS Warehousecode',
             'ivt_adjustment_in.RefNo AS RefNo',
             'ivt_adjustment_in.Remark AS Remark',
             'mst_branch.Code AS BranchsCode',
             'mst_branch.Name AS BranchName',
             'mst_branch.Address AS BranchAddress',
             'mst_branch.CityCode AS BranchCityCode',
             'mst_branch.ZipCode AS BranchZipCode',
             'mst_branch.Phone1 AS BranchPhone1',
             'mst_branch.Phone2 AS BranchPhone2',
             'mst_branch.Fax AS BranchFax',
             'mst_branch.EmailAddress AS BranchEmailAddress',
             'mst_branch.ContactPerson AS BranchContactPerson',
             'mst_branch.DefaultBillToCode AS BranchDefaultBillToCode',
             'mst_branch.DefaultShipToCode AS BranchDefaultShipToCode',
             'mst_branch.ActiveStatus AS BranchActiveStatus',
             'mst_warehouse.Code AS WarehouseCode',
             'mst_warehouse.Name AS WarehouseName',
             'mst_warehouse.WarehouseType AS WarehouseType',
             'mst_warehouse.Address AS WarehouseAddress',
             'mst_warehouse.Phone1 AS WarehousePhone1',
             'mst_warehouse.Phone2 AS WarehousePhone2',
             'mst_warehouse.CountryCode AS WarehouseCountryCode',
             'mst_warehouse.ZipCode AS WarehouseZipCode',
             'mst_warehouse.ActiveStatus AS WarehouseActiveStatus',
       ]);
       $query->leftjoin('mst_branch', 'mst_branch.Code', '=', 'ivt_adjustment_in.BranchCode');
       $query->leftjoin('mst_warehouse', 'mst_warehouse.Code', '=', 'ivt_adjustment_in.WarehouseCode');
       return $query;
    }

    /**
     * Inkombizz | inkombizz@gmail.com | inkombizz.com
     * Set Queries for Search.
     *
     * @param  Illuminate\Database\Eloquent\Builder  $query
     * @param  string  $key
     * @return Illuminate\Database\Eloquent\Builder
     */
    public static function searchId($table, $id) {
        $data = getModelName($table)::select([
          'ivt_adjustment_in.Code AS Code',
          'ivt_adjustment_in.BranchCode AS BranchCode',
          'ivt_adjustment_in.CompanyCode AS CompanyCode',
          'ivt_adjustment_in.Transactiondate AS Transactiondate',
          'ivt_adjustment_in.Warehousecode AS Warehousecode',
          'ivt_adjustment_in.RefNo AS RefNo',
          'ivt_adjustment_in.Remark AS Remark',
          'mst_branch.Code AS BranchsCode',
          'mst_branch.Name AS BranchName',
          'mst_branch.Address AS BranchAddress',
          'mst_branch.CityCode AS BranchCityCode',
          'mst_branch.ZipCode AS BranchZipCode',
          'mst_branch.Phone1 AS BranchPhone1',
          'mst_branch.Phone2 AS BranchPhone2',
          'mst_branch.Fax AS BranchFax',
          'mst_branch.EmailAddress AS BranchEmailAddress',
          'mst_branch.ContactPerson AS BranchContactPerson',
          'mst_branch.DefaultBillToCode AS BranchDefaultBillToCode',
          'mst_branch.DefaultShipToCode AS BranchDefaultShipToCode',
          'mst_branch.ActiveStatus AS BranchActiveStatus',
          'mst_warehouse.Code AS WarehouseCode',
          'mst_warehouse.Name AS WarehouseName',
          'mst_warehouse.WarehouseType AS WarehouseType',
          'mst_warehouse.Address AS WarehouseAddress',
          'mst_warehouse.Phone1 AS WarehousePhone1',
          'mst_warehouse.Phone2 AS WarehousePhone2',
          'mst_warehouse.CountryCode AS WarehouseCountryCode',
          'mst_warehouse.ZipCode AS WarehouseZipCode',
          'mst_warehouse.ActiveStatus AS WarehouseActiveStatus',
                  ])
            ->leftjoin('mst_branch', 'mst_branch.code', '=', 'ivt_adjustment_in.BranchCode')
            ->leftjoin('mst_warehouse', 'mst_warehouse.code', '=', 'ivt_adjustment_in.WarehouseCode')
            ->find(str_replace('%20', ' ', $id));
              return $data;
    }

    /**
     * Inkombizz | inkombizz@gmail.com | inkombizz.com
     * Set Validation From Request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string|null  $type
     * @return Illuminate\Validation\Validator
     */
    public static function validation($request, $type = null) {
        $rules = [
            'BranchCode' => 'nullable|max:250',
            'CompanyCode' => 'nullable|max:250',
            'Transactiondate' => 'nullable|max:250',
            'Warehousecode' => 'nullable|max:250',
            'RefNo' => 'nullable|max:250',
            'Remark' => 'nullable|max:250',
            'CreatedBy' => 'nullable||max:250',
            'CreatedDate' => 'nullable|date_format:Y-m-d H:i:s',
            'UpdatedBy' => 'nullable|max:250',
            'UpdatedDate' => 'nullable|date_format:Y-m-d H:i:s',
            'InActiveBy' => 'nullable|max:250',
            'InActiveDate' => 'nullable|date_format:Y-m-d H:i:s',
        ];

        return Validator::make($request->all(), $rules);
    }

    /**
     * Inkombizz | inkombizz@gmail.com | inkombizz.com
     * Save resource in database.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  object|null  $data
     * @return object
     */
    public static function save($request, $data = null, $table =null) {
        if (is_null($data)) {
            $data = new AdjustmentIn;
        }

        if ($request->Code) {
            $data->Code = $request->Code;
        }else {
            $data->Code = generadeCode($table,"ADJ-IN",null,5);
        }
        if ($request->except('BranchCode')) {
            $data->BranchCode = $request->BranchCode;
        }
        if ($request->except('CompanyCode')) {
            $data->CompanyCode = $request->CompanyCode;
        }
        if ($request->except('Transactiondate')) {
            $data->Transactiondate = $request->Transactiondate;
        }
        if ($request->except('Warehousecode')) {
            $data->Warehousecode = $request->Warehousecode;
        }
        if ($request->except('RefNo')) {
            $data->RefNo = $request->RefNo;
        }
        if ($request->except('Remark')) {
            $data->Remark = $request->Remark;
        }
        if ($request->except('CreatedBy')) {
            $data->CreatedBy = $request->CreatedBy;
        }
        if ($request->except('CreatedDate')) {
            $data->CreatedDate = $request->CreatedDate;
        }
        if ($request->except('UpdatedBy')) {
            $data->UpdatedBy = $request->UpdatedBy;
        }
        if ($request->except('UpdatedDate')) {
            $data->UpdatedDate = $request->UpdatedDate;
        }
        $data->save();

        return $data;
    }

}
