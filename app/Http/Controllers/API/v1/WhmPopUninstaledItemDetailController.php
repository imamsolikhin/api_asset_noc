<?php

namespace App\Http\Controllers\API\v1;

use App\Http\Models\WhmPopUninstaledItemDetail;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class WhmPopUninstaledItemDetailController extends Controller {

    /**
     * Inkombizz | inkombizz@gmail.com | inkombizz.com
     * Set Queries for Search.
     *
     * @param  Illuminate\Database\Eloquent\Builder  $query
     * @param  string  $key
     * @return Illuminate\Database\Eloquent\Builder
     */
    public static function searchQuery($query, $request) {
       $query->select('*');
       // if($request->ActiveStatus){
       //     $query->where('ActiveStatus', '=', $request->ActiveStatus);
       // }
       if ($request->HeaderCode) {
           $query->where('HeaderCode', '=', $request->HeaderCode);
       }
       return $query;
    }

    /**
     * Inkombizz | inkombizz@gmail.com | inkombizz.com
     * Set Queries for Search.
     *
     * @param  Illuminate\Database\Eloquent\Builder  $query
     * @param  string  $key
     * @return Illuminate\Database\Eloquent\Builder
     */
    public static function searchId($table, $id) {
        $data = getModelName($table)::select([
                    '*',
                  ])
                  ->find(str_replace('%20', ' ', $id));
       return $data;
    }

    /**
     * Inkombizz | inkombizz@gmail.com | inkombizz.com
     * Set Validation From Request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string|null  $type
     * @return Illuminate\Validation\Validator
     */
    public static function validation($request, $type = null) {
        $rules = [
            'HeaderCode' => 'nullable|max:250',
            'ItemCode' => 'nullable|max:250',
            'Quantity' => 'nullable|max:250',
            'UnitOfMeasureCode' => 'nullable|max:250',
            'Remark' => 'nullable|max:250',
            'CreatedBy' => 'nullable||max:250',
            'CreatedDate' => 'nullable|date_format:Y-m-d H:i:s',
            'UpdatedBy' => 'nullable|max:250',
            'UpdatedDate' => 'nullable|date_format:Y-m-d H:i:s',
        ];

        return Validator::make($request->all(), $rules);
    }

    /**
     * Inkombizz | inkombizz@gmail.com | inkombizz.com
     * Save resource in database.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  object|null  $data
     * @return object
     */
    public static function save($request, $data = null) {
        if (is_null($data)) {
            $data = new WhmPopUninstaledItemDetail;
        }

        if ($request->except('Code')) {
            $data->Code = $request->Code;
        }
        if ($request->except('HeaderCode')) {
            $data->HeaderCode = $request->HeaderCode;
        }
        if ($request->except('ItemCode')) {
            $data->ItemCode = $request->ItemCode;
        }
        if ($request->except('Quantity')) {
            $data->Quantity = $request->Quantity;
        }
        if ($request->except('UnitOfMeasureCode')) {
            $data->UnitOfMeasureCode = $request->UnitOfMeasureCode;
        }
        if ($request->except('Remark')) {
            $data->Remark = $request->Remark;
        }
        if ($request->except('CreatedBy')) {
            $data->CreatedBy = $request->CreatedBy;
        }
        if ($request->except('CreatedDate')) {
            $data->CreatedDate = $request->CreatedDate;
        }
        if ($request->except('UpdatedBy')) {
            $data->UpdatedBy = $request->UpdatedBy;
        }
        if ($request->except('UpdatedDate')) {
            $data->UpdatedDate = $request->UpdatedDate;
        }
        $data->save();

        return $data;
    }

}
