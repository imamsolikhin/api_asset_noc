<?php

namespace App\Http\Controllers\API\v1;

use App\Http\Models\AdjustmentOut;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class AdjustmentOutController extends Controller {

    /**
     * Inkombizz | inkombizz@gmail.com | inkombizz.com
     * Set Queries for Search.
     *
     * @param  Illuminate\Database\Eloquent\Builder  $query
     * @param  string  $key
     * @return Illuminate\Database\Eloquent\Builder
     */
    public static function searchQuery($query, $request) {
       $query->select([
             'ivt_adjustment_out.Code AS Code',
             'ivt_adjustment_out.BranchCode AS BranchCode',
             'ivt_adjustment_out.CompanyCode AS CompanyCode',
             'ivt_adjustment_out.Transactiondate AS Transactiondate',
             'ivt_adjustment_out.WarehouseCode AS WarehouseCode',
             'ivt_adjustment_out.Refno AS Refno',
             'ivt_adjustment_out.Remark AS Remark',
       ]);
       return $query;
    }

    /**
     * Inkombizz | inkombizz@gmail.com | inkombizz.com
     * Set Queries for Search.
     *
     * @param  Illuminate\Database\Eloquent\Builder  $query
     * @param  string  $key
     * @return Illuminate\Database\Eloquent\Builder
     */
    public static function searchId($table, $id) {
        $data = getModelName($table)::select([
          'ivt_adjustment_in.Code AS Code',
          'ivt_adjustment_in.BranchCode AS BranchCode',
          'ivt_adjustment_in.CompanyCode AS CompanyCode',
          'ivt_adjustment_in.Transactiondate AS Transactiondate',
          'ivt_adjustment_in.Warehousecode AS Warehousecode',
          'ivt_adjustment_in.RefNo AS RefNo',
          'ivt_adjustment_in.Remark AS Remark',
                  ])
            ->find(str_replace('%20', ' ', $id));
              return $data;
    }

    /**
     * Inkombizz | inkombizz@gmail.com | inkombizz.com
     * Set Validation From Request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string|null  $type
     * @return Illuminate\Validation\Validator
     */
    public static function validation($request, $type = null) {
        $rules = [
            'BranchCode' => 'nullable|max:250',
            'CompanyCode' => 'nullable|max:250',
            'Transactiondate' => 'nullable|max:250',
            'WarehouseCode' => 'nullable|max:250',
            'Refno' => 'nullable|max:250',
            'Remark' => 'nullable|max:250',
            'CreatedBy' => 'nullable||max:250',
            'CreatedDate' => 'nullable|date_format:Y-m-d H:i:s',
            'UpdatedBy' => 'nullable|max:250',
            'UpdatedDate' => 'nullable|date_format:Y-m-d H:i:s',
            'InActiveBy' => 'nullable|max:250',
            'InActiveDate' => 'nullable|date_format:Y-m-d H:i:s',
        ];

        return Validator::make($request->all(), $rules);
    }

    /**
     * Inkombizz | inkombizz@gmail.com | inkombizz.com
     * Save resource in database.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  object|null  $data
     * @return object
     */
    public static function save($request, $data = null, $table =null) {
        if (is_null($data)) {
            $data = new AdjustmentOut;
        }

        if ($request->Code) {
            $data->Code = $request->Code;
        }else {
            $data->Code = generadeCode($table,"ADJ-OUT",null,5);
        }
        if ($request->except('BranchCode')) {
            $data->BranchCode = $request->BranchCode;
        }
        if ($request->except('CompanyCode')) {
            $data->CompanyCode = $request->CompanyCode;
        }
        if ($request->except('Transactiondate')) {
            $data->Transactiondate = $request->Transactiondate;
        }
        if ($request->except('WarehouseCode')) {
            $data->WarehouseCode = $request->WarehouseCode;
        }
        if ($request->except('Refno')) {
            $data->Refno = $request->Refno;
        }
        if ($request->except('Remark')) {
            $data->Remark = $request->Remark;
        }
        if ($request->except('CreatedBy')) {
            $data->CreatedBy = $request->CreatedBy;
        }
        if ($request->except('CreatedDate')) {
            $data->CreatedDate = $request->CreatedDate;
        }
        if ($request->except('UpdatedBy')) {
            $data->UpdatedBy = $request->UpdatedBy;
        }
        if ($request->except('UpdatedDate')) {
            $data->UpdatedDate = $request->UpdatedDate;
        }
        $data->save();

        return $data;
    }

}
