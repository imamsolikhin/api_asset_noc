<?php

/**
 * Make a Response.
 *
 * @param  int  $statusCode
 * @param  string  $status
 * @param  string  $message
 * @param  array|object|null  $data
 * @param  array  $headers
 * @return json
 */
function makeResponse($statusCode, $status, $message, $data = null, $headers = []) {
    $result = [
        'status_code' => $statusCode,
        'status' => $status == 'pagination' ? 'success' : $status,
        'message' => $message,
        'data' => $data,
    ];

    if ($status == 'pagination') {
        $result = array_merge($result, ['paginator' => [
                'total_records' => (int) $data->total(),
                'total_pages' => (int) $data->lastPage(),
                'current_page' => (int) $data->currentPage(),
                'per_page' => (int) $data->perPage(),
        ]]);
    }

    return response()->json($result, $statusCode, $headers);
}

/**
 * Make a Key Name.
 *
 * @param  string  $table
 * @return string
 */
function getKeyName($table) {
    return str_replace(' ', '', ucwords(str_replace('-', ' ', $table)));
}

/**
 * Make a Model Name.
 *
 * @param  string  $table
 * @return string
 */
function getModelName($table) {
    return 'App\Http\Models\\' . getKeyName($table);
}

/**
 * Make a Resource Name.
 *
 * @param  string  $table
 * @return string
 */
function getResourceName($table) {
    return 'App\Http\Resources\\' . getKeyName($table);
}

/**
 * Make a Controller Name.
 *
 * @param  string  $table
 * @return string
 */
function getControllerName($table) {
    return 'App\Http\Controllers\API\v1\\' . getKeyName($table) . 'Controller';
}

function generadeCode($table, $branch=Null, $prfix=Null, $numb=5)
{
   $branch = ($branch)? $branch."-":"";
   $prfix = ($prfix)? $prfix."-":"";
   $last_count = getModelName($table)::count()+1;
   $code = $branch.$prfix.str_pad($last_count, $numb, '0', STR_PAD_LEFT);
   return $code;
}

function to_bool($val = null) {
    if ($val == "on" || $val != null) {
        return 1;
    } else {
        return 0;
    }
}
