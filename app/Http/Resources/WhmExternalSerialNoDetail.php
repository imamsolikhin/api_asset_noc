<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class WhmExternalSerialNoDetail extends JsonResource {

    public function toArray($request) {
        return [
            'Code' => $this->Code,
            'HeaderCode' => $this->HeaderCode,
            'ItemCode' => $this->ItemCode,
            'SerialNo' => $this->SerialNo,
            'RemarkText' => $this->RemarkText,
            'CreatedBy' => $this->CreatedBy,
            'CreatedDate' => $this->CreatedDate != null ? date('Y-m-d H:i:s', strtotime($this->CreatedDate)) : null,
            'UpdatedBy' => $this->UpdatedBy,
            'UpdatedDate' => $this->UpdatedDate != null ? date('Y-m-d H:i:s', strtotime($this->UpdatedDate)) : null,
                    ];
    }

}
