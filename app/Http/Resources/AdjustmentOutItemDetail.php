<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class AdjustmentOutItemDetail extends JsonResource {

    public function toArray($request) {
        return [
            'Code' => $this->Code,
            'Headecode' => $this->Headercode,
            'ItemCode' => $this->ItemCode,
            'ReasonCode' => $this->ReasonCode,
            'Quantity' => $this->Quantity,
            'Remark' => $this->Remark,
            'CreatedBy' => $this->CreatedBy,
            'CreatedDate' => $this->CreatedDate != null ? date('Y-m-d H:i:s', strtotime($this->CreatedDate)) : null,
            'UpdatedBy' => $this->UpdatedBy,
            'UpdatedDate' => $this->UpdatedDate != null ? date('Y-m-d H:i:s', strtotime($this->UpdatedDate)) : null,
                    ];
    }

}
