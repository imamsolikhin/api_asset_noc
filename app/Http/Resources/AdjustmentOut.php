<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class AdjustmentOut extends JsonResource {

    public function toArray($request) {
        return [
            'Code' => $this->Code,
            'BranchCode' => $this->BranchCode,
            'CompanyCode' => $this->CompanyCode,
            'Transactiondate' => $this->Transactiondate,
            'WarehouseCode' => $this->WarehouseCode,
            'Refno'=> $this->Refno,
            'Remark'=> $this->Remark,
            'CreatedBy' => $this->CreatedBy,
            'CreatedDate' => $this->CreatedDate != null ? date('Y-m-d H:i:s', strtotime($this->CreatedDate)) : null,
            'UpdatedBy' => $this->UpdatedBy,
            'UpdatedDate' => $this->UpdatedDate != null ? date('Y-m-d H:i:s', strtotime($this->UpdatedDate)) : null,
                    ];
    }

}
