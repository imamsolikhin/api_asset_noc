<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class WhmInternalSerialNoDetail extends JsonResource {

    public function toArray($request) {
        return [
            'Code' => $this->Code,
            'HeaderCode' => $this->HeaderCode,
            'ItemCode' => $this->ItemCode,
            'SerialNo' => $this->SerialNo,
            'InTransactionNo' => $this->InTransactionNo,
            'RemarkText' => $this->RemarkText,
            'RemarkDate' => $this->RemarkDate,
            'RemarkQuantity' => $this->RemarkQuantity,
            'LocationCode'  => $this->LocationCode,
            'CreatedBy' => $this->CreatedBy,
            'CreatedDate' => $this->CreatedDate != null ? date('Y-m-d H:i:s', strtotime($this->CreatedDate)) : null,
            'UpdatedBy' => $this->UpdatedBy,
            'UpdatedDate' => $this->UpdatedDate != null ? date('Y-m-d H:i:s', strtotime($this->UpdatedDate)) : null,
                    ];
    }

}
