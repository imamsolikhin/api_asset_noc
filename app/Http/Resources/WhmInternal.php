<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class WhmInternal extends JsonResource {

    public function toArray($request) {
        return [
            'Code' => $this->Code,
            'Transactiondate' => $this->Transactiondate,
            'BranchCode' => $this->BranchCode,
            'CompanyCode' => $this->CompanyCode,
            'CurrencyCode' => $this->CurrencyCode,
            'ExchangeRate' => $this->ExchangeRate,
            'SourceWarehouseCode' => $this->SourceWarehouseCode,
            'DestinationWarehouseCode' => $this->DestinationWarehouseCode,
            'ApprovalStatus' => $this->ApprovalStatus,
            'ApprovalDate' => $this->ApprovalDate,
            'ApprovalBy' => $this->ApprovalBy,
            'RefNo' => $this->RefNo,
            'Remark' => $this->Remark,
            'CreatedBy' => $this->CreatedBy,
            'CreatedDate' => $this->CreatedDate != null ? date('Y-m-d H:i:s', strtotime($this->CreatedDate)) : null,
            'UpdatedBy' => $this->UpdatedBy,
            'UpdatedDate' => $this->UpdatedDate != null ? date('Y-m-d H:i:s', strtotime($this->UpdatedDate)) : null,
                    ];
    }

}
